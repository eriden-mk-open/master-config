const path = require('path');
const fs = require('fs');
const dotenv = require('dotenv');
const Ajv = require('ajv');
const merge = require('deepmerge');

const ajv = new Ajv({
  $data: true,
  removeAdditional: true,
  useDefaults: true,
  coerceTypes: 'array',
  allErrors: true,
});

module.exports = function(environments, schema, opts = {}) {
  if (!environments) throw new Error('environments param is required');

  const dirname = path.dirname(module.parent.filename);

  for (let env in environments) {
    let exists = fs.existsSync(
      path.join(dirname, `env.${environments[env]}.js`)
    );
    if (!exists)
      throw new Error(
        path.join(dirname, `env.${environments[env]}.js config is required`)
      );
  }

  const options = {
    overwriteVariables: opts.overwriteVariables || ['NODE_ENV'],
    mergeRules: opts.mergeRules || {},
  };

  const config = {
    environments: {
      ...environments,
    },
  };

  const envConfig = dotenv.config();
  if (envConfig.error) throw envConfig.error;

  if (schema) {
    for (let key in schema.properties) {
      if (
        options.overwriteVariables.indexOf(key) === -1 &&
        process.env.hasOwnProperty(key) &&
        !envConfig.parsed.hasOwnProperty(key)
      ) {
        throw new Error(
          `Invalid prop: ${key}. Only parsed props and those allowed in overwriteVariables can be validated and overwritten.`
        );
      }
    }

    if (schema.properties && !schema.properties.NODE_ENV) {
      schema.properties.NODE_ENV = {
        enum: Object.values(config.environments),
      };
    }

    const valid = ajv.validate(schema, process.env);

    if (!valid) {
      throw new Error(
        ajv.errors
          .map(e =>
            e.dataPath
              ? `${e.dataPath} ${e.message}${
                  e.params && e.params.allowedValues
                    ? ': ' + e.params.allowedValues
                    : ''
                }`
              : e.message
          )
          .join('\n')
      );
    }
  }

  config.env = process.env.NODE_ENV;

  const configsToMerge = [];
  configsToMerge.push(config);
  configsToMerge.push(require(path.join(dirname, 'base')));
  if (options.mergeRules[config.env]) {
    configsToMerge.push(
      require(path.join(dirname, `./env.${options.mergeRules[config.env]}`))
    );
  }
  configsToMerge.push(require(path.join(dirname, `./env.${config.env}`)));

  return merge.all(configsToMerge, {
    arrayMerge: (target, source) => source,
  });
};
