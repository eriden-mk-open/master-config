# master-config

Opinionated config merger.
- dotenv is used to load .env into process.env
- ajv is used to validate .env schema
- deepmerge is used to merge base config file with env based config file

## Usage

```js
// ./config/index.js
const masterConfig = require('master-config');

// .config/base.js
const environments = {
  LOCAL: 'local', // ./config/env.local.js
  DEVELOPMENT: 'development', // ./config/env.development.js
  STAGING: 'staging', // ./config/env.production.js
  PRODUCTION: 'production', // ./config/env.local.js
  TEST: 'test', // ./config/env.test.js
};

const schema = {
  type: 'object',
  required: [
    'REDIS_HOST',
    'REDIS_PORT',
    'DB_HOST',
    'DB_PORT',
  ],
  properties: {
    PORT: { type: 'number', default: 3000 },
  },
};

const options = {
  overwriteVariables: ['NODE_ENV'], //process.env.NODE_ENV will be overwritten
  mergeRules: {
    [environments.TEST]: environments.LOCAL, //test env config will be merged with local env config
  }
};

const config = masterConfig(environments, schema, options);

```
